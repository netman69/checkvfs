#include "fuseops.h"
#include "layer0.h"
#include "db.h"

#include <stddef.h> /* NULL */
#include <stdlib.h> /* atexit(), EXIT_FAILURE */
#include <stdio.h> /* fprintf(), stderr */
#include <string.h> /* strdup() */

/* TODO
 *  implement read_buf and write_buf
 *  make sure that checksum is created before requesting/checking it, if file was modified
 *  utime for systems that don't have utimens
 *  lock/flock implementation?
 */

char *db_filename = "checkvfs.db"; // TODO
char *rootpath = NULL;

static int cfs_opt_proc(void *data, const char *arg, int key, struct fuse_args *outargs) {
	 if (key == FUSE_OPT_KEY_NONOPT && rootpath == NULL) {
		rootpath = strdup(arg);
		return 0;
	 }
	 return 1;
}

void onexit(void) {
	db_deinit();
	if (rootpath != NULL)
		free(rootpath);
}

int main(int argc, char *argv[]) {
	struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
	if (atexit(onexit) != 0) {
		fprintf(stderr, "atexit does not want\n");
		return EXIT_FAILURE;
	}
	fuse_opt_parse(&args, NULL, NULL, cfs_opt_proc);
	if (rootpath == NULL) {
		fprintf(stderr, "usage: %s [options] <directory> <mount point>\n", argv[0]);
		return EXIT_FAILURE;
	}
	if (db_init(db_filename) != 0) {
		fprintf(stderr, "can't open database\n");
		return EXIT_FAILURE;
	}
	if (db_file_addtree(rootpath) != 0) { // TODO We want to be able to disable. TODO Also maybe tell what's going on.
		fprintf(stderr, "error scanning tree\n");
		return EXIT_FAILURE;
	}
	// TODO check sqlite3_threadsafe()
	return fuse_main(args.argc, args.argv, &cfs_oper, NULL);
}
