#ifndef __FUSEOPS_H__
#define __FUSEOPS_H__

#include <sys/time.h> /* On some versions fuse.h doesn't work without this. */
#ifdef FUSE3
	#define FUSE_USE_VERSION 31
	#include <fuse3/fuse.h>
#else
	/* We retain fuse 2 support for OpenBSD. */
	#define FUSE_USE_VERSION 26
	#include <fuse/fuse.h>
#endif /* FUSE3 */

extern struct fuse_operations cfs_oper;

#endif /* __FUSEOPS_H__ */
