/* Layer 0
 * 
 * This one wraps all the file-related system calls to add the relative path of the folder we mounted.
 */

#include "layer0.h"
#include "main.h"

#include <stddef.h> /* NULL */
#include <stdlib.h> /* malloc(), free() */
#include <errno.h> /* errno */
#include <string.h> /* memcpy(), memset() */
#include <sys/statvfs.h> /* statvfs() */
#include <sys/types.h> /* lstat(), truncate(), ftruncate() */
#include <sys/stat.h> /* lstat(), chmod(), lchmod(), mkdir() */
#include <unistd.h> /* access(), lchown(), symlink(), readlink(), close(), link(), unlink(), pread(), pwrite(), truncate(), ftruncate(), rmdir() */
#include <fcntl.h> /* open() */
#include <stdio.h> /* rename() */
#include <dirent.h> /* opendir(), readdir(), closedir() */

char *getpath(const char *path) {
	size_t rpl = strlen(rootpath), pl = strlen(path);
	char *r;
	if (rpl + pl <= rpl) /* Who knows? */
		return NULL;
	if ((r = malloc(rpl + pl + 1)) != NULL) {
		memcpy(r, rootpath, rpl);
		memcpy(r + rpl, path, pl);
		r[rpl + pl] = 0;
	}
	return r;
}

int cfs0_statfs(const char *path, struct statvfs *stbuf) {
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (statvfs(p, stbuf) < 0)
		r = -errno;
	free(p);
	return r;
}

int cfs0_getattr(const char *path, struct stat *stbuf, struct fuse_file_info *fi) {
	if (fi != NULL) {
		if (fstat(fi->fh, stbuf) < 0)
			return -errno;
		return 0;
	}
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (lstat(p, stbuf) < 0)
		r = -errno;
	free(p);
	return r;
}

int cfs0_access(const char *path, int mask) {
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (access(p, mask) < 0)
		r = -errno;
	free(p);
	return r;
}

int cfs0_chmod(const char *path, mode_t mode, struct fuse_file_info *fi) {
	if (fi != NULL) {
		if (fchmod(fi->fh, mode) < 0)
			return -errno;
		return 0;
	}
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	#ifdef HAVE_LCHMOD // TODO Is this sensible at all?
		if (lchmod(p, mode) < 0)
			r = -errno;
	#else // TODO Figure out wtf linux does if we chmod symlinks.
		if (chmod(p, mode) < 0) // TODO Apparently POSIX does not support changing permissions of a symlink but some systems do, should think.
			r = -errno;
	#endif /* HAVE_LCHMOD */
	free(p);
	return r;
}

int cfs0_chown(const char *path, uid_t uid, gid_t gid, struct fuse_file_info *fi) {
	if (fi != NULL) {
		if (fchown(fi->fh, uid, gid) < 0)
			return -errno;
		return 0;
	}
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (lchown(p, uid, gid) < 0)
		r = -errno;
	free(p);
	return r;
}

#ifdef HAVE_UTIMENSAT // TODO Do some systems have utimens, but not utimensat?
int cfs0_utimens(const char *path, const struct timespec *ts, struct fuse_file_info *fi) {
	if (fi != NULL) {
		if (futimens(fi->fh, ts) < 0)
			return -errno;
		return 0;
	}
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (utimensat(0, p, ts, AT_SYMLINK_NOFOLLOW) < 0)
		r = -errno;
	free(p);
	return r;
}
#endif /* HAVE_UTIMENSAT */

// TODO Test the to path is correct for both relative and absolute paths.
int cfs0_symlink(const char *from, const char *to) {
	int r = 0;
	char *pf = getpath(from);
	if (pf == NULL)
		return -ENOMEM;
	if (symlink(pf, to) < 0)
		r = -errno;
	free(pf);
	return r;
}

int cfs0_readlink(const char *path, char *buf, size_t size) {
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (readlink(p, buf, size - 1) < 0)
		r = -errno;
	free(p);
	buf[r] = 0;
	return r;
}

int cfs0_mknod(const char *path, mode_t mode, dev_t rdev) {
	if (!S_ISREG(mode)) /* We only allow regular files to be created trough the VFS. */
		return -EOPNOTSUPP;
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (open(p, O_CREAT | O_EXCL | O_WRONLY, mode) < 0)
		r = -errno;
	free(p);
	close(r);
	return r;
}

int cfs0_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (open(p, fi->flags, mode) < 0)
		r = -errno;
	free(p);
	fi->fh = r;
	return (r > 0) ? 0 : r;
}

int cfs0_link(const char *from, const char *to) {
	int r = 0;
	char *pf = getpath(from), *pt = getpath(to);
	if (pf == NULL || pt == NULL)
		return -ENOMEM;
	if (link(pf, pt) < 0)
		r = -errno;
	free(pf);
	free(pt);
	return r;
}

int cfs0_unlink(const char *path) {
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (unlink(p) < 0)
		r = -errno;
	free(p);
	return r;
}

int cfs0_rename(const char *from, const char *to, unsigned int flags) {
	int r = 0;
	char *pf = getpath(from), *pt = getpath(to);
	if (pf == NULL || pt == NULL)
		return -ENOMEM;
	if (flags != 0) // TODO Maybe?
		return -EINVAL;
	if (rename(pf, pt) < 0)
		r = -errno;
	free(pf);
	free(pt);
	return r;
}

int cfs0_open(const char *path, struct fuse_file_info *fi) {
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (open(p, fi->flags, 0) < 0)
		r = -errno;
	free(p);
	fi->fh = r;
	return (r > 0) ? 0 : r;
}

int cfs0_release(const char *path, struct fuse_file_info *fi) {
	if (close(fi->fh) < 0)
		return -errno;
	return 0;
}

int cfs0_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	int fd;
	ssize_t r;
	if (fi == NULL) { /* This does happen. */
		char *p = getpath(path);
		if (p == NULL)
			return -ENOMEM;
		fd = open(p, O_RDONLY, 0);
		free(p);
		if (fd < 0)
			return -errno;
	} else fd = fi->fh;
	if ((r = pread(fd, buf, size, offset)) < 0)
		r = -errno;
	if (fi == NULL)
		close(fd);
	return r;
}

int cfs0_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	int fd;
	ssize_t r;
	if (fi == NULL) { /* This does happen. */
		char *p = getpath(path);
		if (p == NULL)
			return -ENOMEM;
		fd = open(p, O_WRONLY, 0);
		free(p);
		if (fd < 0)
			return -errno;
	} else fd = fi->fh;
	if ((r = pwrite(fd, buf, size, offset)) < 0)
		r = -errno;
	if (fi == NULL)
		close(fd);
	if (r >= 0 && r != (ssize_t) size)
		r = -EIO;
	return r;
}

int cfs0_truncate(const char *path, off_t size, struct fuse_file_info *fi) {
	if (fi != NULL) {
		if (ftruncate(fi->fh, size) < 0)
			return -errno;
		return 0;
	}
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (truncate(p, size) < 0)
		r = -errno;
	free(p);
	return r;
}

int cfs0_fsync(const char *path, int isdatasync, struct fuse_file_info *fi) {
	if (fsync(fi->fh) < 0)
		return -errno;
	return 0;
}

#if defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE)
int cfs0_fallocate(const char *path, int mode, off_t offset, off_t length, struct fuse_file_info *fi) {
	if (mode != 0)
		return -EOPNOTSUPP;
	return -posix_fallocate(fi->fh, offset, length);
}
#endif /* defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE) */

int cfs0_mkdir(const char *path, mode_t mode) {
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (mkdir(p, mode) < 0)
		r = -errno;
	free(p);
	return r;
}

int cfs0_rmdir(const char *path) {
	int r = 0;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	if (rmdir(p) < 0)
		r = -errno;
	free(p);
	return r;
}

int cfs0_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi, int flags) {
	// TODO This can improve a lot!
	DIR *dp;
	struct dirent *de;
	char *p = getpath(path);
	if (p == NULL)
		return -ENOMEM;
	dp = opendir(p);
	free(p);
	if (dp == NULL)
		return -errno;
	while ((de = readdir(dp)) != NULL) {
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = de->d_ino;
		st.st_mode = de->d_type << 12;
		#ifdef FUSE3
			if (filler(buf, de->d_name, &st, 0, 0))
				break;
		#else
			if (filler(buf, de->d_name, &st, 0))
				break;
		#endif /* FUSE3 */
	}
	closedir(dp);
	return 0;
}
