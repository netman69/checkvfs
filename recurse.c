#include "recurse.h"

#include <dirent.h> /* opendir(), readdir(), closedir() */
#include <sys/stat.h> /* lstat() */
#include <string.h> /* memcpy(), strcmp(), strlen() */
#include <stdlib.h> /* malloc(), realloc(), free() */
#include <errno.h> /* errno */
#include <stdint.h> /* size_t */

/* Note that if opendir fails this returns 0 but leaves it unchanged. */
static int pushdir(recurse_item_t **it, const char *dirpath) {
	recurse_item_t *tmp = malloc(sizeof(recurse_item_t));
	if ((tmp) == NULL)
		return -1;
	tmp->path = NULL;
	tmp->dir = NULL;
	tmp->dirpath = dirpath;
	tmp->dirpath_len = strlen(dirpath);
	tmp->parent = *it;
	if ((tmp->dir = opendir(tmp->dirpath)) == NULL) {
		free(tmp);
		return 0; /* We don't stop when opendir fails. */
	}
	*it = tmp;
	return 0;
}

static void popdir(recurse_item_t **it) {
	recurse_item_t *tmp = *it;
	*it = (*it)->parent;
	if (tmp->path != NULL)
		free(tmp->path);
	if (tmp->dir != NULL)
		closedir(tmp->dir);
	free(tmp);
}

static int setpath(recurse_item_t *it, const char *file) {
	size_t nl = strlen(file), pos = it->dirpath_len;
	it->path = realloc(it->path, it->dirpath_len + nl + 2);
	if (it->path == NULL)
		return -1;
	memcpy(it->path, it->dirpath, pos);
	if (pos > 0 && it->path[pos - 1] != '/')
		it->path[pos++] = '/';
	memcpy(it->path + pos, file, nl);
	it->path[pos + nl] = 0;
	it->name = it->path + pos;
	return 0;
}

/* This function recurses a directory tree without following symlinks
 *   it will call fn for every item, first the containing dir,
 *   similar to ftw()/nftw().
 * The function will fail and return -1 with errno set if the directory
 *   given as path can't be opened, if readdir() fails, or if memory
 *   allocation fails.
 * If opendir() or lstat() fails the err argument to fn will hold errno,
 *   otherwise it will be 0. If lstat() fails the stat argument will
 *   be NULL.
 * If the fn function returns anything other than 0 the function will
 *   stop and return the value fn returned.
 */
int recurse(const char *path, int (*fn)(recurse_item_t *it, struct stat *st, int err)) {
	recurse_item_t *it = NULL;
	int ret;

	/* Create the initial item in our stack of directories. */
	if (pushdir(&it, path) != 0 || it == NULL)
		goto error;

	/* Iterate trough the directory tree. */
	do {
		struct dirent *entry;
		while (1) {
			struct stat st;
			errno = 0; /* So we know the difference between error and end of directory. */
			if ((entry = readdir(it->dir)) == NULL)
				break;
			if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
				continue;
			/* Slap the filename and the path of this dir together. */
			if (setpath(it, entry->d_name) != 0)
				goto error;
			/* Stat and if it is a dir, go into it if appropriate. */
			if (lstat(it->path, &st) < 0) {
				if ((ret = fn(it, NULL, errno)) != 0)
					goto bail;
			} else {
				recurse_item_t *cit = it; /* So we can pushdir but keep current item. */
				errno = 0; /* So we can pass along error if opendir() fails. */
				if (S_ISDIR(st.st_mode) && pushdir(&it, it->path) != 0)
					goto error;
				if ((ret = fn(cit, &st, errno)) != 0)
					goto bail;
			}
		}
		if (errno != 0)
			goto error;
		popdir(&it);
	} while (it != NULL);
	return 0;

	/* Free resources and return in case of error. */
error:
	ret = -1;
bail:
	while (it != NULL)
		popdir(&it);
	return ret;
}
