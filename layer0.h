/* Layer 0
 * 
 * This one wraps all the file-related system calls to add the relative path of the folder we mounted.
 */

#ifndef __LAYER0_H__
#define __LAYER0_H__

#include <sys/time.h> /* On some versions fuse.h doesn't work without this. */
#ifdef FUSE3
	#define FUSE_USE_VERSION 31
	#include <fuse3/fuse.h>
#else
	/* We retain fuse 2 support for OpenBSD. */
	#define FUSE_USE_VERSION 26
	#include <fuse/fuse.h>
#endif /* FUSE3 */

extern int cfs0_statfs(const char *path, struct statvfs *stbuf);
extern int cfs0_getattr(const char *path, struct stat *stbuf, struct fuse_file_info *fi);
extern int cfs0_access(const char *path, int mask);
extern int cfs0_chmod(const char *path, mode_t mode, struct fuse_file_info *fi);
extern int cfs0_chown(const char *path, uid_t uid, gid_t gid, struct fuse_file_info *fi);
#ifdef HAVE_UTIMENSAT
extern int cfs0_utimens(const char *path, const struct timespec *ts, struct fuse_file_info *fi);
#endif /* HAVE_UTIMENSAT */
extern int cfs0_symlink(const char *from, const char *to);
extern int cfs0_readlink(const char *path, char *buf, size_t size);
extern int cfs0_mknod(const char *path, mode_t mode, dev_t rdev);
extern int cfs0_create(const char *path, mode_t mode, struct fuse_file_info *fi);
extern int cfs0_link(const char *from, const char *to);
extern int cfs0_unlink(const char *path);
extern int cfs0_rename(const char *from, const char *to, unsigned int flags);
extern int cfs0_open(const char *path, struct fuse_file_info *fi);
extern int cfs0_release(const char *path, struct fuse_file_info *fi);
extern int cfs0_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
extern int cfs0_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
extern int cfs0_truncate(const char *path, off_t size, struct fuse_file_info *fi);
extern int cfs0_fsync(const char *path, int isdatasync, struct fuse_file_info *fi);
#if defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE)
extern int cfs0_fallocate(const char *path, int mode, off_t offset, off_t length, struct fuse_file_info *fi);
#endif /* defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE) */
extern int cfs0_mkdir(const char *path, mode_t mode);
extern int cfs0_rmdir(const char *path);
extern int cfs0_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi, int flags);

#endif /* __LAYER0_H__ */
