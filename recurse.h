#ifndef __RECURSE_H__
#define __RECURSE_H__

#include <stdlib.h> /* size_t */
#include <dirent.h> /* DIR */
#include <sys/stat.h> /* struct stat */

typedef struct recurse_item {
	char *path, *name;
	const char *dirpath;
	size_t dirpath_len;
	DIR *dir;
	int tag; /* Not used by recurse itself, will be retained by parent items. */
	struct recurse_item *parent;
} recurse_item_t;

extern int recurse(const char *path, int (*fn)(recurse_item_t *it, struct stat *stat, int err));

#endif /* __RECURSE_H__ */
