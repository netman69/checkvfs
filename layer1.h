/* Layer 1
 * 
 * This one wraps layer 0 to add logging and bookkeeping.
 */

#ifndef __LAYER1_H__
#define __LAYER1_H__

#include <sys/time.h> /* On some versions fuse.h doesn't work without this. */
#ifdef FUSE3
	#define FUSE_USE_VERSION 31
	#include <fuse3/fuse.h>
#else
	/* We retain fuse 2 support for OpenBSD. */
	#define FUSE_USE_VERSION 26
	#include <fuse/fuse.h>
#endif /* FUSE3 */

extern int cfs1_statfs(const char *path, struct statvfs *stbuf);
extern int cfs1_getattr(const char *path, struct stat *stbuf, struct fuse_file_info *fi);
extern int cfs1_access(const char *path, int mask);
extern int cfs1_chmod(const char *path, mode_t mode, struct fuse_file_info *fi);
extern int cfs1_chown(const char *path, uid_t uid, gid_t gid, struct fuse_file_info *fi);
#ifdef HAVE_UTIMENSAT
extern int cfs1_utimens(const char *path, const struct timespec *ts, struct fuse_file_info *fi);
#endif /* HAVE_UTIMENSAT */
extern int cfs1_symlink(const char *from, const char *to);
extern int cfs1_readlink(const char *path, char *buf, size_t size);
extern int cfs1_mknod(const char *path, mode_t mode, dev_t rdev);
extern int cfs1_create(const char *path, mode_t mode, struct fuse_file_info *fi);
extern int cfs1_link(const char *from, const char *to);
extern int cfs1_unlink(const char *path);
extern int cfs1_rename(const char *from, const char *to, unsigned int flags);
extern int cfs1_open(const char *path, struct fuse_file_info *fi);
extern int cfs1_release(const char *path, struct fuse_file_info *fi);
extern int cfs1_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
extern int cfs1_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi);
extern int cfs1_truncate(const char *path, off_t size, struct fuse_file_info *fi);
extern int cfs1_fsync(const char *path, int isdatasync, struct fuse_file_info *fi);
#if defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE)
extern int cfs1_fallocate(const char *path, int mode, off_t offset, off_t length, struct fuse_file_info *fi);
#endif /* defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE) */
extern int cfs1_mkdir(const char *path, mode_t mode);
extern int cfs1_rmdir(const char *path);
extern int cfs1_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi, int flags);

#endif /* __LAYER1_H__ */
