OBJS = main.o fuseops.o layer1.o layer0.o db.o io.o recurse.o

# Configuration flags:
#   HAVE_UTIMENSAT: utimensat() function exists
#   HAVE_LCHMOD: lchmod() function exists
#   HAVE_POSIX_FALLOCATE: posix_fallocate() function exists
#   FUSE3: fuse version >= 3 (give fuse3 to pkg-config)
CFLAGS  += -g -Wall -Wextra -Wno-unused-parameter -pedantic -std=c99 -D_BSD_SOURCE
CFLAGS  += `pkg-config --cflags sqlite3 fuse3` -DHAVE_UTIMENSAT -DHAVE_POSIX_FALLOCATE -DFUSE3
CFLAGS  += -D_FILE_OFFSET_BITS=64
LDFLAGS += `pkg-config --libs sqlite3 fuse3`

all: checkvfs

checkvfs: $(OBJS)
	$(CC) -o $@ $(OBJS) $(LDFLAGS)

clean:
	rm -rf checkvfs $(OBJS)
