/* Layer 1
 * 
 * This one wraps layer 0 to add logging and bookkeeping.
 */

#include "io.h"
#include "db.h"
#include "layer1.h"
#include "layer0.h"

#include <stdlib.h> /* free() */

int cfs1_statfs(const char *path, struct statvfs *stbuf) {
	return cfs0_statfs(path, stbuf);
}

int cfs1_getattr(const char *path, struct stat *stbuf, struct fuse_file_info *fi) {
	return cfs0_getattr(path, stbuf, fi);
}

int cfs1_access(const char *path, int mask) {
	return cfs0_access(path, mask);
}

int cfs1_chmod(const char *path, mode_t mode, struct fuse_file_info *fi) {
	char *a1 = msprintf("0%o", mode); /* Could return NULL but we just let that go into the DB i guess. */
	char *a2 = ((fi == NULL) ? NULL : msprintf("%d", fi->fh));
	int id = db_log_op("chmod", path, a1, a2, NULL);
	if (a1 != NULL) free(a1);
	if (a2 != NULL) free(a2);
	int r = cfs0_chmod(path, mode, fi);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_chown(const char *path, uid_t uid, gid_t gid, struct fuse_file_info *fi) {
	char *a1 = msprintf("%d", uid);
	char *a2 = msprintf("%d", gid);
	char *a3 = ((fi == NULL) ? NULL : msprintf("%d", fi->fh));
	int id = db_log_op("chown", path, a1, a2, a3);
	if (a1 != NULL) free(a1);
	if (a2 != NULL) free(a2);
	if (a3 != NULL) free(a3);
	int r = cfs0_chown(path, uid, gid, fi);
	db_log_op_ret(id, r);
	return r;
}

#ifdef HAVE_UTIMENSAT // TODO Do some systems have utimens, but not utimensat?
int cfs1_utimens(const char *path, const struct timespec *ts, struct fuse_file_info *fi) {
	char *a1 = msprintf("%lld.%9ld", (long long) ts->tv_sec, ts->tv_nsec);
	char *a2 = msprintf("%lld.%9ld", (long long) ts->tv_sec, ts->tv_nsec);
	char *a3 = ((fi == NULL) ? NULL : msprintf("%d", fi->fh));
	int id = db_log_op("utimens", path, a1, a2, a3);
	if (a1 != NULL) free(a1);
	if (a2 != NULL) free(a2);
	if (a3 != NULL) free(a3);
	int r = cfs0_utimens(path, ts, fi);
	db_log_op_ret(id, r);
	return r;
}
#endif /* HAVE_UTIMENSAT */

int cfs1_symlink(const char *from, const char *to) {
	int id = db_log_op("symlink", to, from, NULL, NULL);
	int r = cfs0_symlink(from, to);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_readlink(const char *path, char *buf, size_t size) {
	return cfs0_readlink(path, buf, size);
}

int cfs1_mknod(const char *path, mode_t mode, dev_t rdev) {
	char *a1 = msprintf("0%o", mode);
	int id = db_log_op("mknod", path, a1, NULL, NULL);
	if (a1 != NULL) free(a1);
	int r = cfs0_mknod(path, mode, rdev);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
	char *a1 = msprintf("0%o", mode);
	int id = db_log_op("create", path, a1, NULL, NULL);
	if (a1 != NULL) free(a1);
	int r = cfs0_create(path, mode, fi);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_link(const char *from, const char *to) {
	int id = db_log_op("link", to, from, NULL, NULL);
	int r = cfs0_link(from, to);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_unlink(const char *path) {
	int id = db_log_op("unlink", path, NULL, NULL, NULL);
	int r = cfs0_unlink(path);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_rename(const char *from, const char *to, unsigned int flags) {
	int id = db_log_op("rename", from, to, NULL, NULL); // TODO flags?
	int r = cfs0_rename(from, to, flags);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_open(const char *path, struct fuse_file_info *fi) {
	int id = db_log_op("open", path, NULL, NULL, NULL);
	int r = cfs0_open(path, fi);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_release(const char *path, struct fuse_file_info *fi) {
	int id = db_log_op("release", path, NULL, NULL, NULL);
	int r = cfs0_release(path, fi);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	char *a1 = msprintf("%lld", (long long) offset);
	char *a2 = msprintf("%lld", (long long) size);
	char *a3 = ((fi == NULL) ? NULL : msprintf("%d", fi->fh));
	int id = db_log_op("read", path, a1, a2, a3);
	if (a1 != NULL) free(a1);
	if (a2 != NULL) free(a2);
	if (a3 != NULL) free(a3);
	int r = cfs0_read(path, buf, size, offset, fi);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	char *a1 = msprintf("%lld", (long long) offset);
	char *a2 = msprintf("%lld", (long long) size);
	char *a3 = ((fi == NULL) ? NULL : msprintf("%d", fi->fh));
	int id = db_log_op("write", path, a1, a2, a3);
	if (a1 != NULL) free(a1);
	if (a2 != NULL) free(a2);
	if (a3 != NULL) free(a3);
	int r = cfs0_write(path, buf, size, offset, fi);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_truncate(const char *path, off_t size, struct fuse_file_info *fi) {
	char *a1 = msprintf("%lld", (long long) size);
	char *a2 = ((fi == NULL) ? NULL : msprintf("%d", fi->fh));
	int id = db_log_op("truncate", path, a1, a2, NULL);
	if (a1 != NULL) free(a1);
	if (a2 != NULL) free(a2);
	int r = cfs0_truncate(path, size, fi);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_fsync(const char *path, int isdatasync, struct fuse_file_info *fi) {
	return cfs0_fsync(path, isdatasync, fi);
}

#if defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE)
int cfs1_fallocate(const char *path, int mode, off_t offset, off_t length, struct fuse_file_info *fi) {
	return cfs0_fallocate(path, mode, offset, length, fi);
}
#endif /* defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE) */

int cfs1_mkdir(const char *path, mode_t mode) {
	char *a1 = msprintf("0%o", mode);
	int id = db_log_op("mkdir", path, a1, NULL, NULL);
	if (a1 != NULL) free(a1);
	int r = cfs0_mkdir(path, mode);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_rmdir(const char *path) {
	int id = db_log_op("rmdir", path, NULL, NULL, NULL);
	int r = cfs0_rmdir(path);
	db_log_op_ret(id, r);
	return r;
}

int cfs1_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi, int flags) {
	return cfs0_readdir(path, buf, filler, offset, fi, flags);
}
