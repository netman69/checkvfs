#include "db.h"
#include "recurse.h"

#define SQLITE_THREAD_SAFE 1 // TODO If we end up not using threads, disable this (performance)!
#include <sqlite3.h>
#include <stddef.h> /* NULL */
#include <stdlib.h> /* free() */
#include <stdio.h> /* fprintf(), stderr */
#include <ftw.h> /* nftw() */

sqlite3 *db = NULL;
/* Note that type field default and log_types here must match DB_LOG_TYPE_* definitions, also for query_log_op. */
const char *query_init =
	"CREATE TABLE IF NOT EXISTS files (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, parent INTEGER, sum TEXT, size INTEGER, name TEXT, path TEXT UNIQUE, isdir TINYINT, sumvalid TINYINT);"
	"CREATE TABLE IF NOT EXISTS log (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, time DATETIME DEFAULT CURRENT_TIMESTAMP, type TINYINT DEFAULT 0, msg TEXT, a0 TEXT DEFAULT NULL, a1 TEXT DEFAULT NULL, a2 TEXT DEFAULT NULL, a3 TEXT DEFAULT NULL, ret INTEGER DEFAULT NULL);"
	"CREATE TABLE IF NOT EXISTS log_types (id INTEGER PRIMARY KEY, name TEXT);"
	"INSERT OR REPLACE INTO log_types (id, name) VALUES (0, 'MESSAGE');"
	"INSERT OR REPLACE INTO log_types (id, name) VALUES (1, 'OPERATION');";
const char *query_log_msg = "INSERT INTO log (msg) VALUES (%Q);";
const char *query_log_op = "INSERT INTO log (type, msg, a0, a1, a2, a3) VALUES (1, %Q, %Q, %Q, %Q, %Q);";
const char *query_log_op_ret = "UPDATE log SET ret = %d WHERE id = %d;";
const char *query_file_add = "INSERT OR IGNORE INTO files (parent, sum, size, name, path, isdir, sumvalid) VALUES (%d, %Q, %lld, %Q, %Q, %d, %d);";

int db_init(char *filename) { /* Should only be used ones for all threads. */
	// TODO Lock the file, maybe?
	// TODO Optionally open read-only with no create.
	// TODO Stop, drop, and roll if the db is of incorrect version.
	if (sqlite3_open_v2(filename, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL) != SQLITE_OK) {
		sqlite3_close(db);
		return -1; // TODO provide way to get error string (sqlite3_errmsg(db))
	}
	char *err_msg;
	int rc = sqlite3_exec(db, query_init, 0, 0, &err_msg);
	if (rc != SQLITE_OK) {
		sqlite3_free(err_msg);
		sqlite3_close(db);
		db = NULL;
		return -1;
	}
	db_log("* Attached to database with RW permissions."); // TODO Only if RW opened.
	return 0;
}

void db_deinit(void) { /* Should only be used once for all threads. */
	if (db != NULL) {
		db_log("* Detaching from database."); // TODO Only if RW opened.
		sqlite3_close(db); /* This does nothing when NULL argument. */
	}
	db = NULL;
	// TODO Unlock the file if i did the locking thing in db_init
}

int db_valog(char *fmt, va_list ap) {
	char *msg, *query, *err_msg;
	int rc;
	msg = sqlite3_mprintf(fmt, ap); /* Doesn't really need error-checking, NULL is allowed to go into DB in case of disaster. */
	query = sqlite3_mprintf(query_log_msg, msg);
	sqlite3_free(msg); /* NULL is allowed. */
	if (query == NULL) {
		fprintf(stderr, "DB error: out of memory\n");
		return -1;
	}
	rc = sqlite3_exec(db, query, 0, 0, &err_msg);
	sqlite3_free(query);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", err_msg);
		sqlite3_free(err_msg);
		return -1;
	}
	return sqlite3_last_insert_rowid(db); // TODO Thread safety! And flush buffer somehow?
}

int db_log(char *fmt, ...) {
	va_list args;
	int r;
	va_start(args, fmt);
	r = db_valog(fmt, args);
	va_end(args);
	return r;
}

int db_log_op(const char *str, const char *a0, const char *a1, const char *a2, const char *a3) {
	char *query, *err_msg;
	int rc;
	query = sqlite3_mprintf(query_log_op, str, a0, a1, a2, a3);
	if (query == NULL) {
		fprintf(stderr, "DB error: out of memory\n");
		return -1;
	}
	rc = sqlite3_exec(db, query, 0, 0, &err_msg);
	sqlite3_free(query);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", err_msg);
		sqlite3_free(err_msg);
		return -1;
	}
	return sqlite3_last_insert_rowid(db); // TODO Thread safety! And flush buffer somehow?
}

int db_log_op_ret(int id, int ret) {
	char *query, *err_msg;
	int rc;
	query = sqlite3_mprintf(query_log_op_ret, ret, id);
	if (query == NULL) {
		fprintf(stderr, "DB error: out of memory\n");
		return -1;
	}
	rc = sqlite3_exec(db, query, 0, 0, &err_msg);
	sqlite3_free(query);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", err_msg);
		sqlite3_free(err_msg);
		return -1;
	}
	return 0;
}

int db_file_add(int parent, const char *sum, long long size, const char *name, const char *path, int isdir, int sumvalid) {
	char *query, *err_msg;
	int rc;
	query = sqlite3_mprintf(query_file_add, parent, sum, size, name, path, isdir, sumvalid);
	if (query == NULL) {
		fprintf(stderr, "DB error: out of memory\n");
		return -1;
	}
	rc = sqlite3_exec(db, query, 0, 0, &err_msg);
	sqlite3_free(query);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", err_msg);
		sqlite3_free(err_msg);
		return -1;
	}
	return sqlite3_last_insert_rowid(db); // TODO Thread safety!
}

int db_file_addtree_file(recurse_item_t *it, struct stat *st, int err) {
	// TODO what if error happens (errno set and possibly stat NULL)
	// TODO Log these events!
	int tag = ((it->parent == NULL) ? 0 : it->parent->tag);
	int r = db_file_add(0, NULL, ((st != NULL) ? st->st_size : 0), it->name, it->path, ((st != NULL) ? (S_ISDIR(st->st_mode) ? 1 : 0) : 0), tag);
	it->tag = ((r >= 0) ? r : 0);
	return (r >= 0) ? 0 : -1;
}

int db_file_addtree(char *path) {
	char *err_msg;
	int rc = sqlite3_exec(db, "BEGIN TRANSACTION;", 0, 0, &err_msg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", err_msg);
		sqlite3_free(err_msg);
		return -1;
	}
	int r = recurse(path, db_file_addtree_file);
	rc = sqlite3_exec(db, "COMMIT;", 0, 0, &err_msg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", err_msg);
		sqlite3_free(err_msg);
		return -1;
	}
	return r;
}
