#include "layer1.h"

#include <sys/time.h> /* On some versions fuse.h doesn't work without this. */
#ifdef FUSE3
	#define FUSE_USE_VERSION 31
	#include <fuse3/fuse.h>
#else
	/* We retain fuse 2 support for OpenBSD. */
	#define FUSE_USE_VERSION 26
	#include <fuse/fuse.h>
#endif /* FUSE3 */

#include <stddef.h> /* NULL */

#ifdef FUSE3
void *cfs_init(struct fuse_conn_info *conn, struct fuse_config *cfg) {
	/* Don't allow fuse to cache attributes and name lookups. */
	cfg->entry_timeout = 0;
	cfg->negative_timeout = 0;
	cfg->attr_timeout = 0;

	/* Don't ignore inode field of stat stuff. */
	cfg->use_ino = 1;

	return NULL;
}
#else
void *cfs_init(struct fuse_conn_info *conn) {
	// TODO Figure out if we have to disable caching also for fuse 2.
	return NULL;
}
#endif /* FUSE3 */

int cfs_statfs(const char *path, struct statvfs *stbuf) {
	return cfs1_statfs(path, stbuf);
}

#ifdef FUSE3
int cfs_getattr(const char *path, struct stat *stbuf, struct fuse_file_info *fi) {
	return cfs1_getattr(path, stbuf, fi);
}
#else
int cfs_getattr(const char *path, struct stat *stbuf) {
	return cfs1_getattr(path, stbuf, NULL);
}
#endif /* FUSE3 */

int cfs_access(const char *path, int mask) {
	return cfs1_access(path, mask);
}

#ifdef FUSE3
int cfs_chmod(const char *path, mode_t mode, struct fuse_file_info *fi) {
	return cfs1_chmod(path, mode, fi);
}
#else
int cfs_chmod(const char *path, mode_t mode) {
	return cfs1_chmod(path, mode, NULL);
}
#endif /* FUSE3 */

#ifdef FUSE3
int cfs_chown(const char *path, uid_t uid, gid_t gid, struct fuse_file_info *fi) {
	return cfs1_chown(path, uid, gid, fi);
}
#else
int cfs_chown(const char *path, uid_t uid, gid_t gid) {
	return cfs1_chown(path, uid, gid, NULL);
}
#endif /* FUSE3 */

#ifdef HAVE_UTIMENSAT // TODO Do some systems have utimens, but not utimensat?
#ifdef FUSE3
int cfs_utimens(const char *path, const struct timespec ts[2], struct fuse_file_info *fi) {
	return cfs1_utimens(path, ts, fi);
}
#else
int cfs_utimens(const char *path, const struct timespec *ts) {
	return cfs1_utimens(path, ts, NULL);
}
#endif /* FUSE3 */
#endif /* HAVE_UTIMENSAT */

int cfs_symlink(const char *from, const char *to) {
	return cfs1_symlink(from, to);
}

int cfs_readlink(const char *path, char *buf, size_t size) {
	return cfs1_readlink(path, buf, size);
}

int cfs_mknod(const char *path, mode_t mode, dev_t rdev) {
	return cfs1_mknod(path, mode, rdev);
}

int cfs_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
	return cfs1_create(path, mode, fi);
}

int cfs_link(const char *from, const char *to) {
	return cfs1_link(from, to);
}

int cfs_unlink(const char *path) {
	return cfs1_unlink(path);
}

#ifdef FUSE3
int cfs_rename(const char *from, const char *to, unsigned int flags) {
	return cfs1_rename(from, to, flags);
}
#else
int cfs_rename(const char *from, const char *to) {
	return cfs1_rename(from, to, 0);
}
#endif /* FUSE3 */

int cfs_open(const char *path, struct fuse_file_info *fi) {
	return cfs1_open(path, fi);
}

int cfs_release(const char *path, struct fuse_file_info *fi) {
	return cfs1_release(path, fi);
}

int cfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	return cfs1_read(path, buf, size, offset, fi);
}

int cfs_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
	return cfs1_write(path, buf, size, offset, fi);
}

#ifdef FUSE3
int cfs_truncate(const char *path, off_t size, struct fuse_file_info *fi) {
	return cfs1_truncate(path, size, fi);
}
#else
int cfs_truncate(const char *path, off_t size) {
	return cfs1_truncate(path, size, NULL);
}

int cfs_ftruncate(const char *path, off_t size, struct fuse_file_info *fi) {
	return cfs1_truncate(path, size, fi);
}
#endif /* FUSE3 */

int cfs_fsync(const char *path, int isdatasync, struct fuse_file_info *fi) {
	return cfs1_fsync(path, isdatasync, fi);
}

#if defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE)
int cfs_fallocate(const char *path, int mode, off_t offset, off_t length, struct fuse_file_info *fi) {
	return cfs1_fallocate(path, mode, offset, length, fi);
}
#endif /* defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE) */

int cfs_mkdir(const char *path, mode_t mode) {
	return cfs1_mkdir(path, mode);
}

int cfs_rmdir(const char *path) {
	return cfs1_rmdir(path);
}

#ifdef FUSE3
int cfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi, enum fuse_readdir_flags flags) {
	return cfs1_readdir(path, buf, filler, offset, fi, flags);
}
#else
int cfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
	return cfs1_readdir(path, buf, filler, offset, fi, 0);
}
#endif /* FUSE3 */

struct fuse_operations cfs_oper = {
	.init       = cfs_init,
	/* Attributes etc. */
	.statfs     = cfs_statfs,
	.getattr    = cfs_getattr,
	.access     = cfs_access,
	.chmod      = cfs_chmod, // TODO log
	.chown      = cfs_chown, // TODO log
#ifdef HAVE_UTIMENSAT
	.utimens    = cfs_utimens, // TODO log
#endif
	/* Symlinks. */
	.symlink    = cfs_symlink, // TODO log
	.readlink   = cfs_readlink, // TODO log
	/* Files and hardlinks. */
	.mknod      = cfs_mknod, // TODO log
	.create     = cfs_create, // TODO log
	.link       = cfs_link, // TODO log
	.unlink     = cfs_unlink, // TODO log
	.rename     = cfs_rename, // TODO log
	.open       = cfs_open, // TODO maybe log
	.release    = cfs_release, // TODO log
	.read       = cfs_read,
	.write      = cfs_write, // TODO log, sometimes
	.truncate   = cfs_truncate, // TODO log
#ifndef FUSE3
	.ftruncate   = cfs_ftruncate, // TODO log
#endif /* FUSE3 */
	.fsync      = cfs_fsync, // TODO log?
#if defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE)
	.fallocate  = cfs_fallocate, // TODO log
#endif /* defined(FUSE3) && defined(HAVE_POSIX_FALLOCATE) */
	/* Directories. */
	.mkdir      = cfs_mkdir, // TODO log
	.rmdir      = cfs_rmdir, // TODO log
	.readdir    = cfs_readdir,
};
