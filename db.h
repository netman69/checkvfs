#ifndef __DB_H__
#define __DB_H__

#include <stdarg.h>

/* These values are hardcoded also in db.c, watch out for that! */
#define DB_LOG_TYPE_MESSAGE   0
#define DB_LOG_TYPE_OPERATION 1

extern int db_init(char *filename); /* Should only be used ones for all threads. */
extern void db_deinit(void); /* Should only be used once for all threads. */
extern int db_valog(char *fmt, va_list ap);
extern int db_log(char *msg, ...); /* Add message to log. */
extern int db_log_op(const char *str, const char *a0, const char *a1, const char *a2, const char *a3);
extern int db_log_op_ret(int id, int ret);
extern int db_file_addtree(char *path);

#endif /* __DB_H__ */
